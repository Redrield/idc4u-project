#include <Arduino.h>

#define PINR 11
#define PING 10
#define PINB 9

#define TAU 2*PI

float r = 0.0;
float g = TAU/3.0;
float b = (2.0 * TAU)/3.0;

void setup() {
  Serial.begin(9600);
  Serial.setTimeout(1000);

  pinMode(PINR, OUTPUT);
  pinMode(PING, OUTPUT);
  pinMode(PINB, OUTPUT);
  Serial.println("xdxdxd screm letsa go");

  digitalWrite(PINR, HIGH);
  digitalWrite(PING, HIGH);
  digitalWrite(PINB, HIGH);
}

void loop() {
  analogWrite(PINR, 127 * sin(r) + 127);
  analogWrite(PING, 127 * sin(g) + 127);
  analogWrite(PINB, 127 * sin(b) + 127);

  r += TAU/100.0;
  g += TAU/100.0;
  b += TAU/100.0;

  delay(50);
}