#include <AccelStepper.h>

#define PINA 9
#define PINB 10
#define PINC 12
#define PIND 13

#define HALFSTEP 8
#define FULLSTEP 4


// AccelStepper stepper(AccelStepper::FULL4WIRE, PINA, PINB, PINC, PIND);
AccelStepper stepper(HALFSTEP, PINA, PINB, PINC, PIND);

void setup() {
   stepper.setMaxSpeed(4000);
   stepper.setSpeed(100);
   stepper.setAcceleration(100);
   stepper.move(8192);
}

void loop() {
   stepper.runSpeed();
}

void resetStepper() {

}